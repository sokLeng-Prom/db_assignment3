<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HelloController;
use App\Http\Controllers\StaffsController;
use App\Http\Controllers\dashboard\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/hw', function () {
//     return view('welcome');
// });

Route::get('/hw', [HelloController::class,'index']);
Route::get('/dashboard', [DashboardController::class,'dashboard']);
Route::get('/add_staffs', [DashboardController::class,'add_staffs']);
Route::post('/save_staffs', [StaffsController::class,'add_staff']);

Route::get('/list_staffs', [StaffsController::class,'list_staff']);
Route::get('/delete_staffs/{id}', [StaffsController::class,'delete_staffs']);
Route::get('/edit_staffs/{id}', [StaffsController::class,'edit_staffs']);
Route::post('update_staffs', [StaffsController::class,'update_staffs']);

