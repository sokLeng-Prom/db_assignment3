<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    function dashboard(){
        return view('home.dashboard');
    }

    function add_staffs(){
        return view('home.add_staffs');
    }
}
