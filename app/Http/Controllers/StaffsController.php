<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffsController extends Controller
{
    function add_staff(Request $request){
        // return $request->vaccination_card;
        $staff = DB::table('staffs')->insert([
            'vaccination_card' => $request->vaccination_card,
            'phone_number' => $request->phone_number,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'id_card' => $request->id_card,
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
        ]);
        // return $staff;

        // return $staff = DB::insert('insert into staffs values (?,?,?,?,?,?,?)',[
        //     $request->vaccination_card,
        //     $request->phone_number,
        //     $request->first_name,
        //     $request->last_name,
        //     $request->id_card,
        //     date('Y-m-d h:i:s'),
        //     date('Y-m-d h:i:s'),]);
        if($staff == 1){
            return redirect('/list_staffs');
        }
    }

    function list_staff(){
        $staffs = DB::table('staffs')->orderBy('created_at','desc')->get();
        // return $staffs = DB::select('select * from staffs order by created_at desc');
        return view('home.list_staffs',compact('staffs'));
    }

    function delete_staffs($id){
        $staff = DB::table('staffs')->where('vaccination_card',$id)->delete();

        // $staff = DB::delete('delete from staffs where vaccination_card = '.$id);
        if($staff == 1){
            return redirect('/list_staffs');
        }
    }

    function edit_staffs($id){
         $staff =
         
         DB::table('staffs')->where('vaccination_card',$id)->get();

         if($staff[0]->vaccination_card==$id){
             return view('home.edit_staffs',compact('staff'));
         };
        // if($staff == 1){
        //     return view('home.edit_staffs');
        // }
        // return $staff = DB::select('select * from staffs where vaccination_card = '.$id);
    }

    function update_staffs(Request $request){
       $staff = DB::table('staffs')->where('vaccination_card',$request->vaccination_card)
        ->update([
            'phone_number' => $request->phone_number,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'id_card' => $request->id_card,
            'updated_at' => date('Y-m-d h:i:s'),
        ]);

        // $staff = DB::statement("Update staffs SET 
        //     phone_number= ".$request->phone_number.",
        //     first_name= ".$request->first_name.",
        //     last_name= ".$request->last_name.",
        //     id_card= ".$request->id_card.",
        //     updated_at= '".date('Y-m-d h:i:s')."'   
        // where vaccination_card = ".$request->vaccination_card." ");
        if($staff == 1){
            return redirect('/list_staffs');
        }
     }
}
