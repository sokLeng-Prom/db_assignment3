<!DOCTYPE html>
<html>
<head>
<title>Edit Staffs</title>
</head>
<body>

<h1>Edit Staffs</h1>
<form action="/update_staffs" method="post">
    @csrf
    <table>
    <input type="hidden" name ="vaccination_card" value='{{$staff[0]->vaccination_card}}'/>
    <tr><td>Phone Number </td><td><input type="text" name ="phone_number" value='{{$staff[0]->phone_number}}'/></td></tr>
    <tr><td>First Name </td><td><input type="text" name ="first_name" value='{{$staff[0]->first_name}}'/></td></tr>
    <tr><td>Last Name </td><td><input type="text" name ="last_name" value='{{$staff[0]->last_name}}'/></td></tr>
    <tr><td>ID Card </td><td><input type="text" name ="id_card" value='{{$staff[0]->id_card}}'/></td></tr>
    <tr><td></td><td><input type="submit" name="button_update" value="Update"/></td></tr>
</form>

</body>
</html>