<h1>All Staffs</h1>
<table border=1>
<tr>
    <td>vaccination Card</td>
    <td>Phone Number</td>
    <td>First Name</td>
    <td>Last Name</td>
    <td>ID Card</td>
    <td>Created At</td>
    <td>Updated At</td>
    <td>Actions</td>
</tr>

@foreach ($staffs as $item)
    
        <tr>
            <td>{{$item->vaccination_card}}</td>
            <td>{{$item->phone_number}}</td>
            <td>{{$item->first_name}}</td>
            <td>{{$item->last_name}}</td>
            <td>{{$item->id_card}}</td>
            <td>{{$item->created_at}}</td>
            <td>{{$item->updated_at}}</td>
            <td><a href='/edit_staffs/{{$item->vaccination_card}}'>Edit</a> | <a href='/delete_staffs/{{$item->vaccination_card}}'>Delete</a></td>

        </tr>
  
@endforeach
</table>